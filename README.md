# OpenDevDTU 固件
> 仓库地址：https://gitee.com/aeo123/OpenDevDTU.git
基于iRTU固件v1.9.3版本，只有增加没有删减功能，目前改动有
1. 对接DevelopLink平台
2. 对接在线参数获取服务器，支持用户自定义任意字段，二次开发利器
3. 增加了rrpc指令
4. 支持基站定位，可在DTU管理页面查看每一个设备的地图坐标或者轨迹
5. 支持DFOTA差分升级，支持升级计划，设备过滤
6. 支持远程补丁功能，轻量级远程启动脚本

## 使用文档地址：[DevelopLink Wiki](http://wiki.developlink.cloud/web/#/5?page_id=46 "DevelopLink Wiki") 
**加入qq群交流反馈，免费定制功能：830407941**
- 在线文档：`http://wiki.developlink.cloud/web/#/5?page_id=46`
- 开源的TCP服务器，用于DTU透传调试，支持linux和win：https://gitee.com/aeo123/tcp_server/releases
- DTU配置工具,自动升级程序和配置页面，完全和合宙一致的表单：`https://gitee.com/aeo123/dtu_serial/releases`,![UI](https://gitee.com/aeo123/dtu_serial/raw/master/ui.jpg)
## 如何使用参数配置功能
注意在线参数配置和平台接入是`两个功能`，不需要同时使用，单独使用参数配置也没问题。
使用说明见在线文档，[DTU在线参数配置平台](http://wiki.developlink.cloud/web/#/5?page_id=52 "DTU在线参数配置平台")

## 如何接入DevelopLink云平台
有待完善...
---

-----------------------------------

## 主要功能

1. 支持TCP/UDP socket,支持HTTP,MQTT,等常见透传和非透传模式
2. 支持OneNET,阿里云，百度云，腾讯云等常见公有云。
3. 支持RTU主控模式
4. 支持数据流模版
5. 支持消息推送(电话，短信，网络通知)
6. 支持GPS数据以及相关数据采集
7. 支持ADC,I2C等外设，可以方便的扩展为屏幕、二维码等解决方案.
8. 需要将配置文件烧录到固件的，修改源码irtu.cfg文件，然后打包源码+lib+core 成固件即可; irtu.cfg 内包含demo，可以用web导出的配置json文件替换''(单引号内的json字符串)即可。

## 相关码云库

1. 合宙Air724U模块, 4G cat.1 https://gitee.com/openLuat/Luat_Lua_Air724U
2. 合宙Air720S模块, 4G cat.4 https://gitee.com/openLuat/Luat_CSDK_Air720S


## Wiki 和 Doc 网站

* http://wiki.openluat.com/
* http://doc.openluat.com/

## 视频教程

* iRTU快速接入教程 https://www.bilibili.com/video/av41012302
* iRTU远程浇花视频 https://www.bilibili.com/video/av47478475
* Luat相关工具教程 https://www.bilibili.com/video/av50453083
* Luat硬件设计参考 https://www.bilibili.com/video/av45341487
* Luat开发视频教程 https://www.bilibili.com/video/av50827315

合宙官网 http://www.openluat.com

## 授权协议

[MIT License](LICENSE)
