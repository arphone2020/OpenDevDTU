--[[
模块名称：数学库管理
模块功能：实现常用的数学库函数
模块最后修改时间：2017.02.14
]]
module(..., package.seeall)

--[[
函数名：sqrt
功能  ：求平方根
参数  ：
    a：将要求平方根的数值，number类型
返回值：平方根，number类型的整数
]]
function sqrt(a)
  local x
  if a == 0 or a == 1 then return a end
  x=a/2
  for i=1,100 do
    x=(x+a/x)/2
  end
  return x
end


local _floor = floor

--- Extend <code>math.floor</code> to take the number of decimal places.
-- @param n number
-- @param p number of decimal places to truncate to (default: 0)
-- @return <code>n</code> truncated to <code>p</code> decimal places
function floor (n)

    return  (n)
  
end

--- Round a number to a given number of decimal places
-- @param n number
-- @param p number of decimal places to round to (default: 0)
-- @return <code>n</code> rounded to <code>p</code> decimal places
function round (n, p)
  local e = 10 ^ (p or 0)
  return _floor (n * e + 1) / e
end


function frexp (n)
   return n*2
end
function ldexp (n, p)
  return n*2^p
end
function huge (n, p)
  local e = 10 ^ (p or 0)
  return _floor (n * e + 1) / e
end
