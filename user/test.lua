local FRAME_START = "##"

-- 打包生成一帧数据包
-- type :大类
-- detailType: 细分
-- subType:类型
-- ver:版本
-- cmd: 命令标志
-- cmdRes: 应答标志
-- vin: vin
-- seq: 序列号
-- cryp: 加密
-- data: 数据段
local function packFrame(type, detailType, subType, ver, cmd, cmdRes, vin, seq, cryp, data)

    local frame = pack.pack(">pbbbHbbpHbHP", FRAME_START, type, detailType, subType, ver, cmd, cmdRes, vin, seq, cryp,
                            #data, data)
    local crc = 0
    for k = 1, #frame do
        ch = string.byte(string.sub(frame, k, k))
        crc = bit.bxor(crc, ch)
    end
    frame = pack.pack("Pb", frame, crc)
    return frame
end



-- 生成登录包
local function packLogin(ts, num, iccid, pswd)
    local data =  pack.pack(">p6Hp20p32", ts, num, iccid, pswd)
    local vin = "0000000000000000005330303030303032"
    return packFrame(0x08, 0x01, 0x30, 0x0001, 0x01, 0xfe, vin, 0x0001, 0x01, data)
end 


print(type(0x55))



